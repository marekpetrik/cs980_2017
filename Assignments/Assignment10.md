# Assignment 10 #

## Problem 1: Simultaneous active parameter estimation and control using sampling-based Bayesian reinforcement learning ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What are Bayesian methods and what are their advantages and disadvantages?
3. What is the computational complexity of solving an MDP, what about a POMDP?

## Problem 2: Predictive Off-Policy Policy Evaluation for Nonstationary Decision Problems, with Applications to Digital Marketing ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. Is a non-stationary MDP just a special case of a regular MDP?
3. What are ARIMA, ARMA, AR, MA?
4. Are there any other good methods for time series prediction?