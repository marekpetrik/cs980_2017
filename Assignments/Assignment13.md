# Assignment 13 #

## Problem 1: Informing sequential clinical decision-making through reinforcement learning: an empirical study ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?


## Problem 2: A Reinforcement Learning System to Encourage Physical Activity in Diabetes Patients ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?
