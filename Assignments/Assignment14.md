# Assignment 14 #

## Problem 1: Controlling blood glucose variability under uncertainty using reinforcement learning and Gaussian processes ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?


## Problem 2: Bayesian Nonparametric Methods for Partially-Observable Reinforcement Learning ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?
