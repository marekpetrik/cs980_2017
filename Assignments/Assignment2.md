# Assignment 2 #

## Problem 1

Imagine that you are looking for the perfect rental apartment. You have clearly formulated your *preferences* in terms of the size, location, condition, and price of the apartment. Using your preferences, you are able to assign a score (such as a real number in the open interval (0-1)) to each apartment that you see and you would like to find one with the *maximal score*. The challenge is that the market is very competitive and if you want to get the particular apartment, you have to make an offer right after you see it. There is also no way of predicting the score that you'd assign to apartments that will come on the market later. You want to make a decision after seeing 5 apartments. 

Formulate this problem as an MDP and compute the optimal *policy* and *value function*. Use either a computer or a pen and paper. Be careful when defining state and action sets. They should be small enough to solve the problem by hand.

**Reading**: See the [secretary problem](https://en.wikipedia.org/wiki/Secretary_problem) on Wikipedia, and Section 3.4.3 in [MDP] which describes the problem and its formulation.

