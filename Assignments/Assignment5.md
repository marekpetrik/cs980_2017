# Assignment 5 #

# Problem 1 #

Use your MDP solver to compute the optimal policy for the following inventory. Download the CSV file of the MDP definition as follows:

1. Open the web application [here](http://rmdp.xyz:3838/cs980/inventory/) or [here](https://rlrl.shinyapps.io/Inventory/)
2. In the tab "Demands" set the seed to 92017
3. Click "New Demands"
4. Go to the tab "Parameters"
5. Click "Construct MDP"
6. Click "Download"

Having solved the MDP, please answer the following questions:

1. Is the solution a base-stock policy? What is the base stock?
2. What happens to the order amount when you increase the discount factor?
3. Try solving the problem with a large discount factor, such as 0.9999. What happens to policies as you keep increasing the discount factor? What happens to the computation time?

## MDP solver options

If your code does not work, try a different solver. Some of the options are as follows:

1. C++, Python, and command line: [CRAAM](https://github.com/marekpetrik/CRAAM). The command line can be used directly with the CSV generated above (see the readme file)
2. R [MDP Toolbox](https://cran.r-project.org/web/packages/MDPtoolbox/MDPtoolbox.pdf)
3. Python [MDP Toolbox](https://github.com/sawcordwell/pymdptoolbox) (Not tested)
4. Ask a class-mate

# Problem 2 #

Read Section 3.1 in ARL (2.1 in the printed version) and Sections 5.1 and 6.1 in RL. Then answer the following questions:

1. What are the conditions for $`\alpha`$ in Algorithm 1 in ARL? Suggest some possible choices.
2. What is the advantage of TD(0) over Monte-Carlo estimation?
3. (optional!) Would you expect TD(0) to be an [unbiased estimator](https://en.wikipedia.org/wiki/Bias_of_an_estimator) of the value function? What about Monte Carlo?
4. (optional!) Would you expect TD(0) to be a [consistent estimator](https://en.wikipedia.org/wiki/Consistent_estimator) of the value function? What about Monte Carlo?
