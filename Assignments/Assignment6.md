# Assignment 6 #

# Problem 1 #

Implement the TD(0) algorithm in Algorithm 1 in ARL. 

The algorithm should accept input in form of samples in a CSV file with the following format:

```
Step,Inventory,Order,Reward,Simulation
1,-0,38,7.599999999999994,Order = 38
2,2,38,-0.47999999999999543,Order = 38
3,-0,36,15.180000000000007,Order = 38
4,-0,38,7.599999999999994,Order = 38
5,-0,38,7.599999999999994,Order = 38
6,-0,38,7.599999999999994,Order = 38
```

The meaning of the columns is as follows:

- *Step*: Order of the step in the simulation. A single file can include several consecutive simulations.
- *Inventory*: Inventory level (= state index)
- *Order*: Amount ordered (= action index)
- *Reward*: Reward received in one step
- *Simulation*: Name of the policy used when generating samples

## Suggestions

- Use C++14/17; it makes things much easier
- My favorite C++ IDE is [QT Creator](https://github.com/qt-creator/qt-creator). Other good options are Code Blocks, Code Lite, Visual Studio Code, and even Eclipse
- Use a library to parse CSV files. [This](https://github.com/ben-strasser/fast-cpp-csv-parser) is a good choice
- [cxxopts](https://github.com/jarro2783/cxxopts) is a good choice for processing command-line parameters

# Problem 2 #

Implement every-visit Monte Carlo (Algorithm 2 in ARL) following the steps above


# Problem 3 #

Test you algorithms on the output from [here](http://rmdp.xyz:3838/cs980/inventory/) or [here](https://rlrl.shinyapps.io/Inventory/). You can generate a CSV with policy samples as follows:

1. Go to the tab "Simulation"
2. Check "Generate samples"
3. Pick and order amount
4. Click "Simulate"
5. Click "Download" 


