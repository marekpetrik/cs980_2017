# Assignment 4 #

Implement a method  for solving a Markov decision process. Please stick with the choice that you indicated in the class, if possible. The options are:

1. Value iteration
2. Policy iteration
3. Modified policy iteration
4. Linear programming

Assume the following representation of the MDP:

- State IDs are numeric, 0-based. An example {0,1,2,3}
- Action IDs are also numeric and 0-based. An example is {0,1} if there are two actions. The number of actions may vary from state to state.
- Rewards depend on state, action, *and the next state*

No need to worry too much about computational complexity. In practice,using sparse representations of the transition probabilities may make a big difference, but that is not the point right now.

## Input

The input is a CSV (comma separated values) file. The columns and a short example are:

```
idstatefrom,idaction,idstateto,probability,reward
0,0,0,0.00412201,0.203178
0,0,1,0.000808912,0.203178
0,0,2,0.00521409,0.203178
0,0,3,0.000731561,0.203178
```

Download a larger example from [here](https://www.dropbox.com/s/qegz78kj7c5gx2q/smallsize_test_nr.csv?dl=0). This is a random MDP and you should not expect that the value function has some special type.

## Output

The output should be a CSV of a deterministic policy, such as

```
idstate,idaction
0,1
1,2
2,0
```

## Hints

When using Python, use Pandas to load and save the CSV file. It is also easy in R using read.csv (and faster using read_csv from the library readr). 


# Test

Test your code on the simple inventory management problem [here](http://rmdp.xyz:3838/cs980/inventory/)