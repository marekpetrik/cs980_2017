# Assignment 11 #

## Problem 1: Optimal monitoring and control under state uncertainty: Application to lionfish management ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What is the belief state? How does it depend on the state space of the POMDP?
3. What does the optimal policy look like in a POMDP? Is it randomized, Markovian, stationary? What about the value function?

## Problem 2: Monte Carlo Motion Planning for Robot Trajectory Optimization Under Uncertainty ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What is the difference between the two variance reduction techniques used in the paper? What is the purpose of variance reduction?
3. Are there any other methods that can be used for variance reduction?
4. What is Julia?