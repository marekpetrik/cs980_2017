# Assignment 3 #

We are continuing with a crash course to get ready for understanding RL. Please, read/skim Chapter 3 in RL and/or Chapter 5 in MDP and **attempt** to answer the following questions (in a reasonable amount of time).

## Problem 1 ##

### Part a
Write down the *Bellman* optimality equation for a *finite-horizon* MDP. 

### Part b
How about when the policy is fixed (or there is only a single action in each state)? This is also called a *Bellman update* equation and it is used, for example, when evaluating a policy in policy iteration.

### Part c
Now, try writing the Bellman update equation using linear algebra:

* $`P_t`$ is the stochastic square matrix of transition probabilities at time $`t`$. $`P_t(i,j)`$ is the probability of transitioning from state $`i`$ to state $`j`$.
* $`r_t`$ is the vector of rewards at time $`t`$. $`r_t(i)`$ is the reward in state $`i`$.

*extra*: Define a problem with a few states (such as 2) and a short horizon (such as 1). Write down the transition probability matrix and rewards vector. 

## Problem 2 ##

### Part a-c
Repeat Problem 1 for an infinite-horizon discounted MDP. Since the transition probabilities and rewards are stationary, what you will need to write the Bellman update equations is:

* $`P`$ is the stochastic square matrix of transition probabilities. $`P(i,j)`$ is the probability of transitioning from state $`i`$ to state $`j`$.
* $`r`$ is the vector of rewards. $`r(i)`$ is the reward in state $`i`$.

### Part d
What does it mean that a matrix is stochastic? What does it mean for its eigenvalues?

### Part e
Use basic linear algebra to solve the Bellman optimality equation for the value function $`v`$.

## Problem 3 ##

### Part a
What is the difference between a stationary and Markovian policy. 

### Part b
Is the optimal policy in finite-horizon MDP guaranteed to be Markovian or stationary (or both)? 

### Part c
What about a discounted infinite-horizon problem?

## Problem 4 ##

### Part a
What happens to the value function when you add a *constant* to all the rewards? 

### Part b
What happens to the policy. How do the answers differ for finite and infinite-horizon problem? 

*Hint*: The problem is easier to answer using the matrix representation of the Bellman update and realizing that $`P e = e`$, where $`e`$ is a vector of all ones. [Neumann series](https://en.wikipedia.org/wiki/Neumann_series) is a very useful tool when dealing with MDPs and you may need it here.


