# Assignment 16 #

## Problem 1: Robot gains Social Intelligence through Multimodal Deep Reinforcement Learning ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?


## Problem 2: Algorithms for Inverse Reinforcement Learning  ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?
