# Assignment 8 #

**Notice**: TD and MC code is in Code/TD. I believe that the implementation is correct. There was a problem with the sample generation which now fixed. Please try your method again.

## Problem 1: TD and Monte-Carlo Mystery ##

Try to generate samples from our little inventory test problem: [here](http://rmdp.xyz:3838/cs980/inventory/) or [here](https://rlrl.shinyapps.io/Inventory/). Notice the changes in the problem parameters; this change also affects the structure of the optimal policy.

You can generate a CSV with policy samples as follows:

1. Go to the tab "Simulation"
2. Check "Generate samples"
3. Pick and order amount
4. Click "Simulate"
5. Click "Download"

I first constructed the full MDP and solved it (using [craam](https://github.com/marekpetrik/craam)):

```bash
$ ./craam-cli -i mdp.csv -o policy.csv -d 0.95 
```

The result is:

| idstate | idaction | value   |
|---------|----------|---------|
| 0       | 66       | 131.477 |
| 1       | 65       | 133.967 |
| 2       | 64       | 136.457 |
| 3       | 63       | 138.947 |
| 4       | 62       | 141.437 |
| 5       | 0        | 144.854 |
| 6       | 0        | 148.801 |
| 7       | 0        | 152.685 |
| 8       | 0        | 156.48  |
| 9       | 0        | 160.156 |
| 10      | 0        | 163.71  |
| 11      | 0        | 167.143 |
| 12      | 0        | 170.461 |
| 13      | 0        | 173.682 |
| 14      | 0        | 176.835 |
| 15      | 0        | 179.953 |
| 16      | 0        | 183.068 |
| 17      | 0        | 186.202 |
| 18      | 0        | 189.367 |
| 19      | 0        | 192.561 |
| 20      | 0        | 195.802 |
| 21      | 0        | 199.061 |
| 22      | 0        | 202.312 |
| 23      | 0        | 205.538 |
| 24      | 0        | 208.71  |
| 25      | 0        | 211.824 |
| 26      | 0        | 214.892 |
| 27      | 0        | 217.926 |


Next, I generated samples from 1000 episodes of 100 steps each using the optimal policy above. To estimate the value function using TD, I ran the following command (see the code in Code/TD subdirectory):

```bash
$ ./MDPSolver -f samples.csv -g 0.95 -a 10 -m TD0 -o tdvalues.csv 
```
The TD estimate of the value function follows:

| idstate | value   | 
|---------|---------| 
| 0       | 129.287 | 
| 1       | 130.056 | 
| 2       | 132.777 | 
| 3       | 134.753 | 
| 4       | 135.309 | 
| 5       | 141.528 | 
| 6       | 145.632 | 
| 7       | 148.507 | 
| 8       | 153.127 | 
| 9       | 156.545 | 
| 10      | 159.485 | 
| 11      | 163.912 | 
| 12      | 166.98  | 
| 13      | 170.245 | 
| 14      | 172.864 | 
| 15      | 176.628 | 
| 16      | 179.574 | 
| 17      | 182.569 | 
| 18      | 185.449 | 
| 19      | 189.528 | 
| 20      | 191.697 | 
| 21      | 194.867 | 
| 22      | 198.789 | 
| 23      | 202.255 | 
| 24      | 205.263 | 
| 25      | 208.03  | 
| 26      | 210.92  | 
| 27      | 213.755 | 

I also estimated the value function using MC as follows (see the code in Code/TD subdirectory):

```bash
$ ./MDPSolver -f samples.csv -a 10 -g 0.95 -m EVMC -o mcvalues.csv
```

The MC estimate of the optimal value function is:

| idstate | value   | 
|---------|---------| 
| 0       | 94.4202 | 
| 1       | 85.2373 | 
| 2       | 85.8403 | 
| 3       | 95.1699 | 
| 4       | 81.6528 | 
| 5       | 112.966 | 
| 6       | 126.015 | 
| 7       | 112.671 | 
| 8       | 112.643 | 
| 9       | 117.437 | 
| 10      | 122.161 | 
| 11      | 129.264 | 
| 12      | 129.705 | 
| 13      | 132.546 | 
| 14      | 140.269 | 
| 15      | 129.878 | 
| 16      | 145.896 | 
| 17      | 135.23  | 
| 18      | 143.732 | 
| 19      | 152.035 | 
| 20      | 155.396 | 
| 21      | 161.172 | 
| 22      | 163.666 | 
| 23      | 168.873 | 
| 24      | 174.94  | 
| 25      | 175.308 | 
| 26      | 176.143 | 
| 27      | 180.292 | 


**Question**: Both MC and TD estimates are decent but they are both lower than the true value function. What are some possible explanations? How would you test them?

## Problem 2: Approximating Value Functions ##

Take the MC value function estimate (or generate your own):

| idstate | value   | 
|---------|---------| 
| 0       | 94.4202 | 
| 1       | 85.2373 | 
| 2       | 85.8403 | 
| 3       | 95.1699 | 
| 4       | 81.6528 | 
| 5       | 112.966 | 
| 6       | 126.015 | 
| 7       | 112.671 | 
| 8       | 112.643 | 
| 9       | 117.437 | 
| 10      | 122.161 | 
| 11      | 129.264 | 
| 12      | 129.705 | 
| 13      | 132.546 | 
| 14      | 140.269 | 
| 15      | 129.878 | 
| 16      | 145.896 | 
| 17      | 135.23  | 
| 18      | 143.732 | 
| 19      | 152.035 | 
| 20      | 155.396 | 
| 21      | 161.172 | 
| 22      | 163.666 | 
| 23      | 168.873 | 
| 24      | 174.94  | 
| 25      | 175.308 | 
| 26      | 176.143 | 
| 27      | 180.292 | 
| 28      | 193.266 | 
| 29      | 188.549 | 
| 30      | 160.153 | 
| 31      | 182.635 | 
| 32      | 191.628 | 
| 33      | 185.812 | 
| 34      | 207.821 | 
| 35      | 201.36  | 
| 36      | 214.379 | 
| 37      | 206.205 | 
| 38      | 216.187 | 
| 39      | 209.672 | 
| 40      | 224.692 | 
| 41      | 225.556 | 
| 42      | 221.654 | 
| 43      | 206.715 | 
| 44      | 234.158 | 
| 45      | 224.737 | 
| 46      | 243.967 | 
| 47      | 234.546 | 
| 48      | 235.39  | 
| 49      | 242.918 | 
| 50      | 252.5   | 
| 51      | 246.885 | 
| 52      | 253.141 | 
| 53      | 253.051 | 
| 54      | 255.129 | 
| 55      | 259.826 | 
| 56      | 268.333 | 
| 57      | 262.223 | 
| 58      | 257.521 | 
| 59      | 268.022 | 
| 60      | 267.412 | 
| 61      | 278.415 | 
| 62      | 272.703 | 
| 63      | 272.959 | 
| 64      | 278.84  | 
| 65      | 252.245 | 
| 66      | 253.105 | 

The try approximating the above value function using linear regression and the following types of features (described in ARL 3.2). 

1. Linear function
2. Polynomials
3. State aggregation with 3 state groups
4. Tile coding
5. Linear splines

Plot the target function and its approximation. 

**Hint**:
The following Python (3.6+) code implements the repression in part 1: 
```python
import numpy as np
y = # value function above
A = np.column_stack((np.ones(67), np.arange(67))) # a single linear feature and the intercept

beta = np.linalg.solve( (A.T @ A), A.T @ y)
y_approx = A @ beta
 # plot y, y_approx
```

