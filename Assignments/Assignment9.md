# Assignment 9 #

## Problem 1: Residual Algorithms: Reinforcement Learning with Function Approximation ##


1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What does the star example show?
3. What is the goal of residual gradient algorithms?
4. What does the Hall problem show?
5. Does the paper analyze on-policy or off-policy algorithms?
6. Do you have any ideas for improvements?


## Problem 2: The Linear Programming Approach to Approximate Dynamic Programming ##


1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What does Theorem 2 show? Why does it matter?
3. How is Theorem 3 better? What is its main limitation?
4. How would you improve on this work?
