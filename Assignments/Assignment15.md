# Assignment 15 #

## Problem 1: Towards Monocular Vision based Obstacle Avoidance through Deep Reinforcement Learning ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?


## Problem 2: Target-driven visual navigation in indoor scenes using deep reinforcement learning ##

1. Write a short summary of the paper. What is the problem that it solves, what is the main contribution and how is it achieved?
2. What do you find interesting about the paper?
