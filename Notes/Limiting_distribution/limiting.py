import numpy as np

P = np.array([[0,0.5,0.5,0],[0,1,0,0],[0,0,0.5,0.5], [0,0,0.4,0.6]])

E,F = np.linalg.eig(P.T)

# Q = P.T
Q = np.linalg.inv(F) @ (P.T) @ F

Pinf_t = F @ np.diag([(0 if i < 1 else 1) for i in E]) @ np.linalg.inv(F)

Pinf = Pinf_t.T

print(Pinf)
