# CS980: Advanced Machine Learning #

### Where ###

Kingsbury N233

### When ###

MW  3:40pm - 5:00pm

## Schedule ##

| **Date** | **Day** |           **Topic**            |       **Reading**        |              **Assignment**              |
| -------- | ------- | :----------------------------: | :----------------------: | :--------------------------------------: |
| Aug 28   | Mon     |          Introduction          |                          |                                          |
| Aug 30   | Wed     |              MDPs              |    ARL Ch. 2, MDP 1,2    | [1](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment1.md) |
| Sep 04   | Mon     |    **Labor Day** (no class)    |     MDP 3 (optional)     |                                          |
| Sep 06   | Wed     |      MDP: Finite horizon       |          MDP 4           | [2](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment2.md) |
| Sep 11   | Mon     |     MDP: Infinite horizon      |       RL 3, MDP 5        | [3](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment3.md) |
| Sep 13   | Wed     |    MDP: Discounted rewards     |          MDP 5           |                   None                   |
| Sep 18   | Mon     |    Linear programs for MDPs    |          MDP 5           |                   None                   |
| Sep 20   | Wed     |  MDP: Total & Average rewards  |         MDP 7,8          | [4](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment4.md) |
| Sep 25   | Mon     |  Temporal difference learning  |   ARL 3.1 (2.1 print)    | [5](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment5.md) |
| Sep 27   | Wed     |  Temporal difference learning  |       RL 5.1, 6.1        | [6](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment6.md) |
| Oct 02   | Mon     |  Project presentations and TD  |           None           |                   None                   |
| Oct 04   | Wed     |  Value function approximation  |  ARL 3.2 (up to 3.2.1)   | [7](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment7.md) |
| Oct 09   | Mon     |     TD and approximations      |        ARL 3.2.1         | [8](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment8.md) |
| Oct 11   | Wed     |           LSTD/ LSPE           |        ARL 3.2.3         |                   None                   |
| Oct 16   | Mon     |       Sarsa / Q-learning       |       RL 6.4, 6.5        | Read about [LSPI](http://jmlr.csail.mit.edu/papers/v4/lagoudakis03a.html) |
| Oct 18   | Wed     |          Actor-Critic          |  RL 6.6, ARL 4.4 (3.4)   |                                          |
| Oct 23   | Mon     |  Off-policy/on-policy and ALP  |      Jordan, Marek       | [9](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment9.md) |
| Oct 25   | Wed     |     Bayesian and Robust RL     |      Tianyi, Reazul      | [10](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment10.md) |
| Oct 30   | Mon     |  Partial observability and MC  |      Talha, Yuncong      | [11](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment11.md) |
| Nov 01   | Wed     | Project progress presentations |                          |                                          |
| Nov 06   | Mon     |          Future topic          |                          |                                          |
| Nov 08   | Wed     |      Glucose applications      |      Colin E, Tarun      | [12](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment12.md) |
| Nov 13   | Mon     |      Glucose applications      |      Andrew, Nithin      | [13](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment13.md) |
| Nov 15   | Wed     |          Applications          |      Colin C, Ajesh      | [14](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment14.md) |
| Nov 20   | Mon     |            Deep RL             |     Yishan, Andreas      | [15](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment15.md) |
| Nov 22   | Wed     |  **Thanksgiving** (no class)   |                          |                                          |
| Nov 27   | Mon     |          **No class**          |                          |                                          |
| Nov 29   | Wed     |            Deep RL             | Sangeeta, Mostafa, Marek | [16](https://gitlab.com/marekpetrik/cs980_2017/blob/master/Assignments/Assignment16.md) |
| Dec 04   | Mon     |     Project presentations      |                          |                                          |
| Dec 06   | Wed     |     Project presentations      |                          |                                          |

## Student Paper Presentations ##

| Student             | Topic                 | Paper Title                              | Link                                     |
| ------------------- | --------------------- | ---------------------------------------- | ---------------------------------------- |
| Jordan Ramsdell     | Value Approximation   | Residual Algorithms: Reinforcement Learning with Function Approximation | http://www.leemon.com/papers/1995b.pdf   |
| Marek Petrik        | Approximate LP        | The Linear Programming Approach to Approximate Dynamic Programming | http://pubsonline.informs.org/doi/pdf/10.1287/opre.51.6.850.24925 |
| Tianyi Gu           | Bayesian RL           | Simultaneous active parameter estimation and control using sampling-based Bayesian reinforcement learning | https://arxiv.org/abs/1707.09055         |
| Reazul Russel       | Robust RL             | Predictive Off-Policy Policy Evaluation for Nonstationary Decision Problems, with Applications to Digital Marketing | http://psthomas.com/papers/Thomas2017.pdf |
| Talha Siddique      | Partial observability | Optimal monitoring and control under state uncertainty: Application to lionfish management | http://www.sciencedirect.com/science/article/pii/S0095069616302625 |
| Yuncong Zhou        | Monte Carlo           | Monte Carlo Motion Planning for Robot Trajectory Optimization Under Uncertainty | https://arxiv.org/abs/1504.08053         |
| Collin Crowell      | Application – Cell    | Bayesian Nonparametric Methods for Partially-Observable Reinforcement Learning | http://ieeexplore.ieee.org/document/6616533/ |
| Collin Etzel        | Application – Glucose | Personalized Tuning of Reinforcement Learning Control Algorithm for Glucose Regulation | https://www.ncbi.nlm.nih.gov/pubmed/24110480 |
| Tarun Pandian       | Application – Glucose | Glucose Level Control Using Temporal Difference Methods | http://ieeexplore.ieee.org/document/7985166/ |
| Andrew Mitchell     | Application – Glucose | Informing sequential clinical decision-making through reinforcement learning: an empirical study | https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3143507/ |
| Nithin Sivakumar    | Application – Glucose | A Reinforcement Learning System to Encourage Physical Activity in Diabetes Patients | https://arxiv.org/abs/1605.04070         |
| Ajesh Vijayaragavan | Application – Glucose | Controlling blood glucose variability under uncertainty using reinforcement learning and Gaussian processes | http://www.sciencedirect.com/science/article/pii/S1568494615003932 |
| Yishan Luo          | Deep RL               | Towards Monocular Vision based Obstacle Avoidance through Deep Reinforcement Learning | https://arxiv.org/abs/1706.09829         |
| Andreas Lydakis     | Deep RL               | Target-driven visual navigation in indoor scenes using deep reinforcement learning | https://arxiv.org/abs/1609.05143         |
| Sangeeta Patnaik    | Deep RL               | Robot gains Social Intelligence through Multimodal Deep Reinforcement Learning | https://arxiv.org/abs/1702.07492         |
| Mostafa Hussein     | Inverse RL            | Algorithms for Inverse Reinforcement Learning | http://ai.stanford.edu/~ang/papers/icml00-irl.pdf |


## Books ##

Follow the links for free online versions.

Our main references will be:

- [ARL]: Szepesvári, C. (2010). [Algorithms for Reinforcement Learning](https://sites.ualberta.ca/~szepesva/RLBook.html). 
- [MDP]: Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. John Wiley & Sons, Inc.
- [RL]: Sutton, R. S., & Barto, A. (1998). [Reinforcement learning](http://incompleteideas.net/sutton/book/ebook/the-book.html). MIT Press.

We will also read research papers, which will be posted in the schedule.


## Assignments ##

We will work on the assignments in the class. 

## What ##

In this seminar, we will cover *reinforcement learning*, or how to make good decisions driven by data. The goal in reinforcement learning is to learn how to act while interacting with a dynamic and complex environment. Reinforcement learning methods can be applied in various domains, such as when managing ecosystems, optimizing website, to robotics, and healthcare.

Our focus will be on reinforcement learning that can learn from batch data sets without interacting with the environment. The algorithms have to learn how to interact with the environment based on a historical data. These batch methods are important when the cost of failure is high, such as in healthcare of agriculture, when using trial and error is impractical.

Some of the topics that we will cover are:

- Markov decision processes: Policy iteration, Value iteration, Linear programming
- Policy evaluation (online and offline): TD, LSTD
- Batch policy improvement: Q-learning, LSPI
- Convex optimization: Linear programming

The focus of the class will be on depth rather than breadth. The main goal of the class is delivering an interesting reinforcement learning group project. As much as possible we will work on the same code-base using a subset of C++, Python, and R. The class will require independent study of reading materials and in-class group problem solving and paper discussions.


## Grading ##

The final grade will be composed of:

- 30% Assignments: Each assignment is complete/incomplete. Collaboration on the assignments is encouraged. We will discuss the solutions in class. 
- 70% Project outcome and contribution


