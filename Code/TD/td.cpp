// Copyright: 2017 Andreas Lydakis

#include <iostream>
#include <fstream>
#include <set>
#include <cmath>
#include <vector>
#include "cxxopts.hpp"
#include "csv.h"

using namespace std;

enum class Method_t {
    TD0, EVMC
};

int main(int argc, char *argv[]) {

    cxxopts::Options options(argv[0], "Simple TD and MC computation.");

    options.add_options()
            ("h,help", "Display help message.")
            ("f,file", "File name", cxxopts::value<std::string>()->default_value("samples.csv"), "")
            ("o,output", "File name", cxxopts::value<std::string>()->default_value("values.csv"), "")
            ("g, gamma", "Gamma", cxxopts::value<double>()
                    ->default_value("0.9"), "G")
            ("a, alpha", "Alpha", cxxopts::value<double>()
                    ->default_value("1"), "A")
            ("m, method", "Algorithm to use", cxxopts::value<std::string>()
                    ->default_value("TD0"), "M");

    try {
        options.parse(argc, argv);
    } catch (cxxopts::OptionException oe) {
        cout << oe.what() << endl << endl << " *** usage *** " << endl;
        cout << options.help() << endl;
        return -1;
    }

    // print help
    if(options["h"].as<bool>()){
        cout << options.help() << endl;
        return 0;
    }

    std::string file = options["file"].as<std::string>();

    double gamma = options["gamma"].as<double>();
    double alpha = options["alpha"].as<double>();
    std::string m = options["method"].as<std::string>();

    Method_t method;
    if (m == "TD0") {
        method = Method_t::TD0;
    } else if (m == "EVMC") {
        method = Method_t::EVMC;
    } else{
        cout << "Unknown optimization method " << m << ". Terminating." << endl;
        terminate();
    }

    std::cout << "Gamma: " << gamma << std::endl;
    std::cout << "Alpha: " << alpha << std::endl;

    std::cout << "Reading: " << file << std::endl;

    // value function
    vector<double> valuef(0, 0.0);


    if (method == Method_t::TD0) {
        // state and action variables
        int step, inv, ord;
        int cur_inv, cur_ord;
        // rewards
        double rew, cur_rew;

        io::CSVReader<4> in(file);
        in.read_header(io::ignore_extra_column, "Step", "Inventory", "Order", "Reward");

        // iteration number
        int iter = 1;

        // read initial input
        in.read_row(step, cur_inv, cur_ord, cur_rew);

        // parse the input file
        while (in.read_row(step, inv, ord, rew)) {
            // do not update the last step of an episode
            if (step != 1) {
                //std::cout << step << " " << inv << " " << ord << " " << rew << " " << sim << " " << std::endl;
                int maxstate = max(cur_inv, inv);
                if(size_t(maxstate) >= valuef.size()){
                    valuef.resize(maxstate + 1, 0.0);
                }
                valuef[cur_inv] += (alpha / std::sqrt(iter)) * (cur_rew + gamma * valuef[inv] - valuef[cur_inv]);
                iter++;
            }            
            cur_inv = inv;
            cur_rew = rew;
            cur_ord = ord;
        }

        // print statistics
        std::cout << valuef.size() << " States" << std::endl;
    }
    else if (method == Method_t::EVMC) {
        int step, inv, ord;

        double rew;
        std::string sim;

        using State_Action =  pair<int, int> ;
        using Ep_Step = pair<State_Action, double>;
        using Episode = vector<Ep_Step> ;


        map<int, double> values;


        io::CSVReader<4> in(file);
        in.read_header(io::ignore_extra_column, "Step", "Inventory", "Order", "Reward");

        // caches the value of the most-recent episode to replay it backwards
        Episode episode;
        // parse input file - cache episodes to parse them backwards
        bool read_row;
        int iter = 1;
        do{
            read_row = in.read_row(step, inv, ord, rew);

            if (step == 1 || !read_row) {
                // the first step of the next episode, update the values of the most-recent episode
                // reverse the episode order; just to make it convenient to use for range
                reverse(episode.begin(), episode.end());

                // reward is cumulative
                double reward = 0;

                for(const Ep_Step& epstep : episode){
                    auto [state, action] = epstep.first;
                    // update the reward for the last action
                    reward = epstep.second + gamma * reward;

                    if(size_t(state) >= valuef.size())
                        valuef.resize(state + 1, 0.0);
                    valuef[state] += (alpha / std::sqrt(iter)) * (reward - valuef[state]);
                    iter++;
                }
                // reset the episode
                episode.clear();
            }

            if(read_row)
                episode.emplace_back(State_Action(inv, ord), rew);

        } while(read_row);

        std::cout << valuef.size() << " States" << std::endl;
        std::cout << "Gamma: " << gamma << std::endl;
        std::cout << "Alpha: " << alpha << std::endl;
    }

    // only do this when an output file has been provided
    if(options.count("output") > 0){
        cout << "Writing output ... " << endl;
        ofstream ofs;
        ofs.open(options["output"].as<string>());
        if(!ofs.is_open()){
            cout << "Could not open the output file for writing" << endl;
            return -1;
        }
        ofs << "idstate,value" << endl;
        for(size_t i = 0; i < valuef.size(); i++){
            ofs << i << "," << valuef[i] << endl;
        }
        ofs.close();
    }
    cout << "Done." << endl;

}
