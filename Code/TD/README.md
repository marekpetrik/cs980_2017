TD and Monte-Carlo

# Compile

The included compilation script is based on cmake. You should be also able to compile the file td.cpp directly.

The compilation switches necessary as -pthread and -std=c++17. A small modification will make the code compile with C++11 too. 

```bash
$ cmake .
$ make
```

# Execution

To get help, run: 

```bash
./MDPSolver - h
```
