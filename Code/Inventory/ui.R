library(shiny)

# Define UI for application that draws a histogram
shinyUI(fluidPage(
  
    # Application title
    headerPanel("Inventory Management"),
    helpText("Optimize policy for managing inventory with stationary demands."), 
    tabsetPanel(
        # *********** Demands *****************
        tabPanel("Demands",
            sidebarLayout(
                sidebarPanel(
                    numericInput("demandSamples", label = "Number of demands sampled", value = 7),
                    numericInput("demandStd", label = "Demand std", value = 8),
                    numericInput("demandMeanPriorMean", label = "Demand Prior Mean", value = 10 ),
                    numericInput("demandMeanPriorStd", label = "Demand Prior Std", value = 5),
                    numericInput("seed", value = NULL, label = "Demand Seed", min = 0, max = 100000),
                    helpText("Generate a new demand function and a random sample"),
                    actionButton("generatedemand", label = "New Demands")
                ),
                mainPanel(
                    h2("Sampled Demands"),
                    downloadButton("downloadDemands", label = "Download"),
                    tableOutput("observeddemands")
                )
            )
        ),
        # ******* Parameters ****************
        tabPanel("Parameters",
             sidebarLayout(
                sidebarPanel(
                    numericInput("cost", value = 2.49, label = "Purchase Cost ($)", width = '100%', min = 0, step = 0.01),
                    numericInput("price", value = 3.99, label = "Sale Price ($)", width = '100%', min = 0, step = 0.01),
                    numericInput("fixed", value = 29.99, label = "Fixed Delivery Cost ($)", width = '100%', min = 0, step = 0.01),
                    numericInput("holding", value = 0.03, label = "Holding Cost ($)", width = '100%', min = 0, step = 0.01),
                    numericInput("backlog", value = 0.15, label = "Backlog Cost ($)", width = '100%', min = 0, step = 0.01),
                    numericInput("inventory_max", value = 100, label = "Max Inventory", width = '100%', min = 0, step = 1),
                    numericInput("backlog_max", value = 0, label = "Max Backlog", width = '100%', min = 0, step = 1),
                    numericInput("order_max", value = 100, label = "Max Order", width = '100%', min = 0, step = 1),
                    actionButton("constructMDP", label = "Construct MDP"),
                    downloadButton("downloadMDP", label = "Download"),
                    actionButton("computeOptimal", label = "Solve Newsvendor")
                ),
                mainPanel(
                    helpText("Construct MDP first, and then download it.", tags$br(),
                             "States: inventory levels", tags$br(),
                             "Actions: purchase level")
                )
             )),
        # ******* Simulation ****************
        tabPanel("Simulation",
             sidebarLayout(
                 sidebarPanel(
                     numericInput("horizon", value = 100, label = "Horizon", min = 5, max = 1000),
                     numericInput("epochs", value = 100, label = "Epochs", min = 1, max = 1000),
                     numericInput("discount", value = 0.95, label = "Discount", min = 0, max = 1),
                     sliderInput("orderwhen", value = 200, label = "Order if less or equal", min = 0, max = 200),
                     sliderInput("order", value = 10, label = "Order up to", width = "100%", min = 0, max = 200),
                     sliderInput("randomize", value = 0, min = 0, max = 1.0, width = "100%", label = "Randomize (epsilon)", step = 0.1),
                     actionButton("simulate", label = "Simulate"),
                     actionButton("resetsimulation", label = "Reset"),
                     checkboxInput("generateSamples", label = "Generate Samples", value = FALSE),
                     downloadButton("downloadSamples", label = "Download Samples")
                 ),
                 mainPanel(
                    tabsetPanel(
                    tabPanel("Profit",
                        plotOutput("money")),
                    tabPanel("Inventory",
                        plotOutput("inventory"))
                    )
                 )
             )     
        )
    )
    

))
