#
library(shiny)
library(ggplot2)
library(readr)
library(dplyr)

source("simulation.R")

# Define server logic required to draw a histogram
shinyServer(function(input, output, session) {
    
    # parameters that define the problem
    parameters = list(
        demand.mean = NULL,
        demand.sd = NULL,
        demand.sample = c(),
        price.sell = NULL,
        cost.buy = NULL,
        cost.holding = NULL,
        cost.backlog = NULL,
        cost.fixed = NULL,
        inventory.max = NULL,
        backlog.max = NULL,
        order.max = NULL
        )
    
    mdp <- NULL
    
    # updates simulation parameters from the form input
    updateParameters <- function(){
        parameters$price.sell <<- input$price
        parameters$cost.buy <<- input$cost
        parameters$cost.holding <<- input$holding
        parameters$cost.backlog <<- input$backlog
        parameters$cost.fixed <<- input$fixed
        parameters$inventory.max <<- max(as.integer(floor(input$inventory_max)),0)
        parameters$backlog.max <<- max(as.integer(floor(input$backlog_max)),0)
        parameters$order.max <<- max(as.integer(floor(input$order_max)),0)
    }

    # generate empty results of the simulation
    emptyResults <- function(){
        return( data.frame(Day = c(), Wealth = c(), Simulation = c()) )
    }
    emptySamples <- function(){
        return(data.frame(Day = c(), Inventory = c(), Order = c(),  Reward = c(), Simulation = c()))
    }
    
    # **** Simulation results ****
    # Dataframe that contains the results from the simulation
    simulation.result <- reactiveValues(data = emptyResults())
    simulation.samples <- emptySamples()
    
    
    # generate new true demand
    generate.demand <- function(seed, sampleCount, prior.demand.mean, prior.demand.sd){
        # set the seed for the reproducibility of the generated demand
        set.seed(seed)
        parameters$demand.mean <<- rnorm(1, prior.demand.mean, prior.demand.sd)
        parameters$demand.sd <<- parameters$demand.mean / 3
        
        parameters$demand.sample <<- sample.demand(sampleCount, parameters)
        sample.data <- data.frame(Demand = parameters$demand.sample)
        output$observeddemands <- renderTable(sample.data)
        
        # reset simulation results
        simulation.result$data <- emptyResults()
        simulation.samples <- emptySamples()
        set.seed(NULL)
    }
    
    # these values should come from the input instead
    generate.demand(NULL, 7, 10, 5)
    
    # Generate new demand values when chosen
    observeEvent(input$generatedemand, {
        if ( is.integer(input$seed) ) {
            seed <- input$seed
        } else {
            seed <- NULL
        }
        generate.demand(seed, input$demandSamples, input$demandMeanPriorMean, input$demandMeanPriorStd)
    })
    
    # Build MDP definition
    observeEvent(input$constructMDP, {
        withProgress(message = "Building MDP", value = 0, {
            updateParameters()
            mdp <<- build.mdp(parameters = parameters, function(x){incProgress(x)})
        })    
    })
    
    # Download MDP definition
    output$downloadMDP <- downloadHandler(
        filename = "mdp.csv",
        content = function(file) {
            if (!is.null(mdp)) {
                write_csv(mdp, file)
            } else {
                showModal(modalDialog(
                    title = "MDP not built",
                    easyClose = TRUE,
                    helpText("Construct the MDP before downloading it.")
                ))
                write("Construct the MDP before downloading it.", file)
        }
    })
    
    # Run simulation
    observeEvent(input$simulate, {
        withProgress(message = "Running simulation", value = 0, {            
            simulation.length <- input$horizon
            simulation.epochs <- input$epochs
            
            updateParameters()
            
            policy <- policy.ss.randomized(input$orderwhen, input$order, input$randomize)
            
            cat("Running simulation with these parameters:\n")
            print(parameters)
            
            if(input$randomize == 0){
                runname <- paste("s = ", input$orderwhen, " S = ", input$order)
            }else{
                runname <- paste("s = ", input$orderwhen, " S = ", input$order, " e = ", input$randomize)
            }
            
            
            # intialize lists to hold simulation results
            wealth.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            inventory.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            
            # intialize lists to hold samples
            reward.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            order.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            sold.results <- matrix(nrow = simulation.length, ncol = simulation.epochs)
            
            for (epoch in 1:simulation.epochs) {
                # update progress every 100 epochs sampled
                if (epoch %% 100 == 0)
                    incProgress(100/simulation.epochs)
                
                sim.result <- simulate(horizon = simulation.length, policy = policy, 
                                       parameters = parameters, discount = input$discount)
                
                wealth.results[,epoch] <- sim.result$wealth
                inventory.results[,epoch] <- sim.result$inventories
                reward.results[,epoch] <- sim.result$rewards
                order.results[,epoch] <- sim.result$orders
                sold.results[,epoch] <- sim.result$sold
            }
          
            wealth.mean <- apply(wealth.results, 1, mean)
            wealth.diff <- apply(wealth.results, 1, sd) / sqrt(epoch)
            
            inventories.mean <- apply(inventory.results, 1, mean)
            inventories.diff <- apply(inventory.results, 1, sd) / sqrt(epoch)
            
            # remove the data from the previous run
            if (nrow(simulation.result$data) > 0) {
                simulation.result$data <- 
                    simulation.result$data[simulation.result$data$Simulation != runname,]
                if (input$generateSamples) {
                    simulation.samples <<- simulation.samples[simulation.samples$Simulation != runname, ]
                }
            }
            
            # generate samples if applicable
            if (input$generateSamples) {
                simulation.samples <<- rbind(simulation.samples, 
                                            data.frame(Step = 1:simulation.length,
                                                       Inventory = as.vector(inventory.results),
                                                       Order = as.vector(order.results),
                                                       Reward = as.vector(reward.results),
                                                       Sold = as.vector(sold.results),
                                                       Simulation = runname))
            }
            
            # append results to the general storage
            simulation.result$data <- rbind(simulation.result$data, data.frame(
                Day = 1:simulation.length,
                Wealth = wealth.mean, 
                    Wealth.Min = wealth.mean - wealth.diff, 
                    Wealth.Max = wealth.mean + wealth.diff,
                Inventory = inventories.mean, 
                    Inventory.Min = inventories.mean - inventories.diff,
                    Inventory.Max = inventories.mean + inventories.diff,
                Simulation = runname))  
        })
    })
    
    # resets simulation
    observeEvent(input$resetsimulation,{
        simulation.result$data <<- emptyResults()
        simulation.samples <<- emptySamples()
    })
    
    # plot money in the bank
    output$money <- renderPlot({
        if (nrow(simulation.result$data) > 0) {
            ggplot(simulation.result$data, aes(
                    x = Day, y = Wealth, ymin = Wealth.Min, ymax = Wealth.Max, 
                        color = Simulation, group = Simulation, fill = Simulation)) +
                geom_line()  +
                geom_ribbon(alpha = 0.3) +
                theme_linedraw(base_size = 20)
        }
    }, height = 500)
    
    output$inventory <- renderPlot({
        if (nrow(simulation.result$data) > 0) {
            ggplot(simulation.result$data, aes(
                x = Day, y = Inventory, ymin = Inventory.Min, ymax = Inventory.Max, 
                    color = Simulation, group = Simulation, fill = Simulation)) +
                geom_line() +
                geom_ribbon(alpha = 0.3) +
                theme_linedraw(base_size = 20)
        }
    })
    
    # compute optimal threshold
    observeEvent(input$computeOptimal, {
        data <- parameters$demand.sample
        demand.mean.est <- mean(data)
        demand.sd.est <- sd(data)
        
        # update const paramters
        updateParameters()
        
        q.true <- as.integer(round(qnorm(
            (parameters$price.sell - parameters$cost.buy) /
                parameters$price.sell, parameters$demand.mean, parameters$demand.sd)))
        q.est <- as.integer(round(qnorm(
            (parameters$price.sell - parameters$cost.buy) / 
                parameters$price.sell, demand.mean.est, demand.sd.est)))
        
        output.data <- data.frame(labels = c("Demand mean", "True mean", 
                                             "Demand sd", "True sd", 
                                             "Estimated opt order", "True opt order"),
                                 values = c(demand.mean.est, parameters$demand.mean, 
                                            demand.sd.est, parameters$demand.sd, 
                                            q.est, q.true  ))
        
        showModal(modalDialog(
            title = "Optimal Threshold (estimated model)",
            easyClose = TRUE,
            renderTable(output.data, colnames = FALSE )
        ))
        
    })
    
    # demands download handler
    output$downloadDemands <- downloadHandler(
        filename = "demands.csv",
        content = function(file){
            write_csv(data.frame(Demand = parameters$demand.sample), file)
        }
    )
    
    # sample download handler
    output$downloadSamples <- downloadHandler(
        filename = "samples.csv",
        content = function(file){
            write_csv(simulation.samples, file)
        }
    )
    
})
